# Milestone 1 Test Guide
  
### Go Setup on Ubuntu 20.04/Ubuntu 22.04 and Ego Setup
```
sudo apt install golang-1.20
```

### Test all

``` bash
git clone  https://github.com/integritee-network/worker && cd worker
go test ./...
```

